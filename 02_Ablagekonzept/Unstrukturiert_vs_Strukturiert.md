# Unstrukturiert vs Strukturiert

## Zugriff auf Daten
 - Einträge neu erfassen
 - Einträge mutieren (ändern) (Suchen)
 - Einträge lesen    (Suchen)
 - Einträge löschen  (Suchen)

## Unstrukturiert
 -  -> Bsp Sammlung mit Visitenkarten

## Wenig Strukturiert
 -  -> Telefonbuch (Nur nach einem Kriterium Sortiert)

## Strukturiert
 -  -> DB nach verschiedenen Kriterien durchsuchbar

## Datentypen: (<>Variable!)
- Zahlentyp-1: 1,2,3,4... -> Ganze Zahlen / INTEGER (INT) 
- Zahlentyp-2: Decimalzahlen  1.54, 234987.3456 -> Fliesskommazahlen / FLOAT
                              +/- 0.xxxxx E+/-yyyyyy
- Wahr / Falsch: Boolean (BOOL)
- Text: String
- Einzelner Buchstabe: Character (CHAR)