# Ablagekonzept

## Exceltabellen 
 - [Mala_Kean.xlsx](Mala_Kean.xlsx)
 - [Diagramm_Produktivität_Mala_Kean.xlsx](Diagramm_Produktivität_Mala_Kean.xlsx)
 - [Diagramm_Tauchgang_Mala_Kean.xlsx](Diagramm_Tauchgang_Mala_Kean.xlsx)
 - [Diagramme_3.Aufgabe_Mala_Kean.xlsx](Diagramme_3.Aufgabe_Mala_Kean.xlsx)
 - [Diagramme_Manipulieren_Mala_Kean.xlsx](Diagramme_Manipulieren_Mala_Kean.xlsx)

## Unstrukturiert vs Strukturiert
 - [Unstrukturiert vs Strukturiert.md](Unstrukturiert_vs_Strukturiert.md)

## Excel Datenbereich
 - [ExcelDatenbereich.jpg](ExcelDatenbereich.jpg)
  
## Tabellenterminologie & Adressierung
 - [Tabellenterminologie.jpg](Tabellenterminologie.jpg)
 - [Tabellenadressierung.jpg](Tabellenadressierung.jpg)

  
