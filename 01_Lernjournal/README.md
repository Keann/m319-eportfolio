# Lernjournal


## Grundbegriffe
### Begriffserklärung von Daten
 - Daten: Sind Nachrichten, die maschiniell verarbeitet und gespeichert werden können.
 - Nachricht: Wird unterschieden nach syntaktischem, semantischem und pragmatischem Aspekt. Übermittelte Daten sind Nachrichten.
 - Information: Ist in einer Nachricht enthalten und hat einen Neuigkeitswert.
 - Wissen: Bedeutet verknüpfte Information - Assoziationen.
 - Redundanz:
   - Redundanz (Informationstheorie), ohne Informationsverlust wegzulassende Informationen.
   - Redundanz (Kommunikationstheorie), mehrfache Nennung derselben Information.
   - Redundante Informationen besitzen keinen Neuigkeitswert und sind in der Datenverarbeitung unerwünscht!
   - Redundanz (Technik), zusätzliche technische Ressourcen als Reserve wie z.B. ein zusätzliches Netzteil.



### Zugriff auf Daten
  - Einträge neu erfassen
  - Einträge mutieren (ändern) (Suchen)
  - Einträge lesen    (Suchen)
  - Einträge löschen  (Suchen)

### Unstrukturiert
  -  -> Bsp Sammlung mit Visitenkarten

### Wenig Strukturiert
  -  -> Telefonbuch (Nur nach einem Kriterium Sortiert)

### Strukturiert
  -  -> DB nach verschiedenen Kriterien durchsuchbar

### Datentypen: (<>Variable!)
 - Zahlentyp-1: 1,2,3,4... -> Ganze Zahlen / INTEGER (INT) 
 - Zahlentyp-2: Decimalzahlen  1.54, 234987.3456 -> Fliesskommazahlen / FLOAT
                               +/- 0.xxxxx E+/-yyyyyy
 - Wahr / Falsch: Boolean (BOOL)
 - Text: String
 - Einzelner Buchstabe: Character (CHAR)

#### Diskrete, stetige und häufbare Merkmale
  - Häufbares Merkmal: Ein Merkmal kann mehrere Ausprägungen annehmen.
 Bsp.: Personenbefragung → Hobby: Die Person kann unter Hobby mehrere Nennungen machen. (Lesen, Tanzen, Skifahren)
 Bsp.: Berufsausbildung → Die Person hat verschieden Ausbildungen absolviert. (Kochlehre, Hotelfachschule)
 Muss in tabellarische Struktur überführt werden.
  - Diskretes Merkmal: Merkmal hat abzählbar viele Ausprägungen bzw. die Anzahl der möglichen Ausprägungen ist endlich.
  Bsp.: Geschlecht, Haarfarbe, Anzahl der Teilnehmer/innen an einer Veranstaltung etc.
  - Stetiges Merkmal: Merkmal kann unendlich viele Ausprägungen annehmen.
  Bsp.: Wasserpegel, Gewicht, Strecke, Zeitintervall.

### Skalentypen
 - Nominalskala: Meist diskret, häufbar. Daten können untereinander verglichen aber in keine natürliche Reihenfolge gebracht werden. Bsp.: Geschlecht, Kontonummer, Haarfarbe, Telefonnummer, Geschmacksrichtung.
 - Ordinalskala: Meist diskret, nicht häufbar. Können im Gegensatz zu nominalskalierten Daten in eine natürliche Reihenfolge gebracht werden. Die Abstände zwischen den einzelnen Werten sind nicht quantifizierbar. Berechenbar mit Vorbehalt.
Bsp.: Präferenzrangfolgen, Zufriedenheit (z.B. auf einer Skala von 1 bis 5), Dienstrang.
 - Kardinalskala (Metrische Skala): Meist stetig, nicht häufbar. Verfügen über eine natürliche Reihenfolge und quantifizierbaren Abständen. Berechenbar.
Bsp.: Zeitdauer, Wassertiefe, Streckenlänge.

### Datentypen und Wertebereiche
#### Datentypen:
 - Der Datentyp sagt aus, wie die im Computer binär vorliegenden bzw. verarbeitbaren Daten zu interpretieren sind. Die allgemeinen Datentypen:
 - Integer: Ganzzahl, ursprünglich 16 Bit = 216 Kombinationen und somit -32'768 .. + 32'767, Operationen: + - * / < > = % (%=Modulo)
 - Boolean: Wahr oder Falsch bzw. 1 oder 0, Operationen: NOT AND OR = (und Kombinationen davon)
 - Character: Alphanumerisches Zeichen, Satzzeichen (ASCII), Operationen: < > = StrToInt
 - Floating-point numbers: Fliesskommazahl oder Dezimalzahl in der Form ±0.xxx E±yyy, Operationen: + - * / < > =
 - Alphanumeric string: Zeichenkette oder Array of char
 - Weitere spezielle und zusammengesetzte Datentypen:
 -  Enumeration: Aufzählungstypen wo der Wertebereich selber definiert werden kann (z.B. Rot, Grün, Gelb, Blau, Schwarz, Weiss)
 -  Record: Struktur, die ihrerseits verschiedene Datentypen enthält
 -  Einige der zusätzlichen Zahlenformate in z.B. Excel:
 -  Currency: Währung
 -  Date: Datum
 -  Time: Zeit
 -  Prozent

#### Wertebereiche
 - Abgesehen davon, dass Datentypen wie der Integer keine unendlichen Wertebereiche haben, kann auch die Aufgabe eine Einschränkung der einzugebenden Daten erfordern. Zum Beispiel beginnen die Schweizer Postleitzahlen bei 1000 (Lausanne, VD) und enden bei 9658 (Wildhaus, SG). Somit kann man schon bei der Eingabe durch die Einschränkung «1000≤PLZ≤9658» verhindern, dass inexistente Postleitzahlen erfasst werden. (Plausibilität)

#### Datentypen in Excel
   - So werden im Tabellenkalkulationsprogramm EXCEL Datentypen definiert und auch gleich im Wertebereich eingeschränkt: - [Datenbereich](/02_Ablagekonzept/ExcelDatenbereich.jpg)

#### Tabellenterminologie & Adressierung
 - Diese Begriffe muss man kennen: [Tabellenterminologie](/02_Ablagekonzept/Tabellenterminologie.jpg)
 - Und so werden Daten in einer Tabelle gefunden (Dreistufige Adressierung): [Tabellenadressierung](/02_Ablagekonzept/Tabellenadressierung.jpg)


## Zusammengesetzter Primärschlüssel
 - Ein Zusammengesetzter Schlüssel macht nicht immer Sinn. Ein zusammengesetzter Primärschlüssel kann nur einmal vorkommen und setzt sich aus zwei Schlüssel aus zwei unterschiedlichen Tabellen. Die zwei Schlüssel können somit nur einmal in kombination stehen. 
 [Bei diesem Beispiel macht das Sinn.](/02_Ablagekonzept/macht%20sinn.png)

 Jeder Student kann nur einmal in Kombination mit einem Professor stehen. 
 [Bei diesem Beispiel wäre dies jedoch falsch:](/02_Ablagekonzept/bei%20diesem%20beispiel%20falsch.png)

 Es ist falsch weil, jeder Schüler in einer Klasse an einem anderen Wohnort wohnen müsse, damit der Eintrag möglich ist.



## DB-Transaktionen
 - Transaktionen sind Abfragen, mutationen etc. SQL ist eine Abfragesprache und dient zur   strukturierten Abfrage, Aktualisierung, Datendefinition, Datenüberprüfung, Sicherung der Konsistenz und des gleichzeitigen Zugriffs, ferner zur Pflege des Datenkataloges und vieles mehr.
 DML -> Data Manipulation Language
 [Dient zur Abfrage von Daten](/02_Ablagekonzept/dml.png) 
 
 Auf LibreOffice : 
 Abfragen -> Abfrage unter Verwendung des Assistenten erstellen
 Abfragen -> Abfrage unter Entwurfsansicht erstellen
 Abfragen -> Abfrage in SQL-Ansicht erstellen 
 [Ansicht LibreOffice](/02_Ablagekonzept/dml%20libreoffice.png)


## Wildcard:
- dir *ts     



## Cross Join
### Kartesisches Produkt der Tabellen. Verknüpft jeden Datensatz der einen Tabelle mit den Datensätzen der anderen Tabelle:
 - SELECT spalte_x, spalte_y
 - FROM tabelle1 CROSS JOIN tabelle2;


### Cross Join durch ein Komma ersetzen ergibt das selbe:
 - SELECT spalte_x, spalte_y
 - FROM tabelle1 , tabelle2;




## Left Join 
 - Gibt alle Datensätze von der ersten Spalte aus auch wenn diese keinen Datensatz aus der zweiten Spalte hat.
 - [Beispiel:](/02_ablagekonzept/leftjoin.png)

 - Die Tabellen sind über die Job ID verbunden. Eine Person kann nur einen Job haben aber ein Job kann von mehreren Personen ausgeführt werden.
 - [Beispiel:](/02_ablagekonzept/leftjoin2.png)
 - Meier Tim hat hier keinen Job, alle anderen haben einen Job welchen man über die Job ID identifizieren kann.
 
 - Code:
 SELECT Nachname, Beschreibung
 FROM Personen LEFT JOIN Job
 ON Personen.FP_Job = Job.P_Job; 

 - In LibreOffice Base 
 SELECT Personen.Nachname , Job.Beschreibung
 FROM { oj Personen LEFT OUTER JOIN Job ON Personen.FP_Job = Job.P_Job }


 - [Ausgabe:](/02_ablagekonzept/ausgabe.png)

 - Alle Angestellten werden ausgegeben auch wenn sie keinen Job haben. Die Beschreibung bleibt einfach leer, wo es keine hat.


## Right Join
 - Alle Datensätze aus der zweiten Spalte auch wenn sie keinen Datensatz in der ersten Spalte haben.

 [Beispiel:](/02_ablagekonzept/rightjoin1.png)
  - Die Tabellen sind über die Job ID verbunden. Eine Person kann nur einen Job haben aber ein Job kann von mehreren Personen ausgeführt werden. 
 Meier Tim hat keinen Job und niemand hat den Job KV oder Manager.

 - Codes:
 SELECT Nachname, Beschreibung
 FROM Personen RIGHT JOIN Job
 ON Personen.FP_Job = Job.P_Job; 


In LibreOffice:
SELECT Personen.Nachname , Job.Beschreibung
FROM { oj Personen RIGHT OUTER JOIN Job ON Personen.FP_Job = Job.P_Job }




Ausgabe:

Trotz dem, dass keiner KV oder Manager ist werden diese angezeigt. Meier Tim wird jedoch nicht angezeigt da er keinen Job hat.



Inner Join (natural Join):
Nur Personen mit Job und nur Jobs mit Personen werden ausgegeben.
Beispiel:

Die Tabellen sind über die Job ID verbunden. Eine Person kann nur einen Job haben aber ein Job kann von mehreren Personen ausgeführt werden. 



Meier Tim hat keinen Job und niemand hat den Job KV oder Manager.

Code:
SELECT Nachname, Beschreibung
FROM Personen INNER JOIN Job
ON Personen.FP_Job = Job.P_Job; 

(Der Code ist in LibreOffice gleich)

Ausgabe:

Der Job KV, Manager und die Person Tim Meier wird nicht ausgegeben.



Equi Join (cross join)
Nur Personen mit Job und nur Jobs mit Personen werden ausgegeben.
(Durch cross join gibt es grosse zwischentabellen welche den Server vielleicht zum abstürzen bringen)
Beispiel

Die Tabellen sind über die Job ID verbunden. Eine Person kann nur einen Job haben aber ein Job kann von mehreren Personen ausgeführt werden. 



Meier Tim hat keinen Job und niemand hat den Job KV oder Manager.

Code:
SELECT Nachname, Beschreibung
FROM Personen, Job WHERE Personen.FP_Job = Job.P_Job;





Ausgabe:

Die Ausgabe ich gleich wie beim inner Join. Der Job KV, Manager und die Person Tim Meier wird nicht ausgegeben.
